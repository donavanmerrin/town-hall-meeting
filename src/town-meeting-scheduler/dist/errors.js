"use strict";
exports.__esModule = true;
exports.MissingMeetingError = void 0;
var MissingMeetingError = /** @class */ (function () {
    function MissingMeetingError(message) {
        this.description = "This error means a meeting could not be found";
        this.message = message;
    }
    return MissingMeetingError;
}());
exports.MissingMeetingError = MissingMeetingError;
