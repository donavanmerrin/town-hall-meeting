"use strict";
exports.__esModule = true;
exports.Meetings = void 0;
var Meetings = /** @class */ (function () {
    function Meetings(meeting_id, location, time, topic) {
        this.meeting_id = meeting_id;
        this.location = location;
        this.time = time;
        this.topic = topic;
    }
    return Meetings;
}());
exports.Meetings = Meetings;
