"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var errors_1 = require("./errors");
var meetings_services_impl_1 = require("./services/meetings-services-impl");
var cors = require('cors');
var app = (0, express_1["default"])();
app.use(express_1["default"].json());
app.use(cors());
var meetingsService = new meetings_services_impl_1.MeetingsServiceImpl();
app.get('/meetings', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var meetings;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, meetingsService.retrieveAllMeetings()];
            case 1:
                meetings = _a.sent();
                res.send(meetings);
                res.status(200);
                return [2 /*return*/];
        }
    });
}); });
app.get('/meetings/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var meetingId, meeting, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                meetingId = Number(req.params.id);
                return [4 /*yield*/, meetingsService.retieveMeetingById(meetingId)];
            case 1:
                meeting = _a.sent();
                res.send(meeting);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                if (error_1 instanceof errors_1.MissingMeetingError) {
                    res.status(404);
                    res.send(errors_1.MissingMeetingError);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put('/meetings/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var auth, meetingId, meet, updatedMeeting, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                auth = req.headers.authorization;
                if (auth !== "funky_bandit") {
                    res.status(405);
                    res.send("Not authorized to perform this action");
                }
                meetingId = Number(req.params.id);
                meet = req.body;
                meet.meeting_id = meetingId;
                return [4 /*yield*/, meetingsService.updateMeeting(meet)];
            case 1:
                updatedMeeting = _a.sent();
                return [4 /*yield*/, meetingsService.updateMeeting(updatedMeeting)];
            case 2:
                _a.sent();
                res.send(updatedMeeting);
                res.status(200);
                return [3 /*break*/, 4];
            case 3:
                error_2 = _a.sent();
                if (error_2 instanceof errors_1.MissingMeetingError) {
                    res.status(404);
                    res.send(errors_1.MissingMeetingError);
                }
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.post('/meetings', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var auth, meeting, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                auth = req.headers.authorization;
                if (auth !== "funky_bandit") {
                    res.status(405);
                    res.send("Not authorized to perform this action");
                }
                meeting = req.body;
                return [4 /*yield*/, meetingsService.createMeeting(meeting)];
            case 1:
                meeting = _a.sent();
                res.send(meeting);
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                if (error_3 instanceof errors_1.MissingMeetingError) {
                    res.status(404);
                    res.send(errors_1.MissingMeetingError);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app["delete"]('/meetings/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var auth, meetingId, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                auth = req.headers.authorization;
                if (auth !== "funky_bandit") {
                    res.status(405);
                    res.send("Not authorized to perform this action");
                }
                meetingId = Number(req.params.id);
                return [4 /*yield*/, meetingsService.deleteMeeting(meetingId)];
            case 1:
                _a.sent();
                res.send(meetingId + " has been deleted.");
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                if (error_4 instanceof errors_1.MissingMeetingError) {
                    res.status(404);
                    res.send(errors_1.MissingMeetingError);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.listen(process.env.PORT || 3001, function () {
    console.log("Application Started!");
});
