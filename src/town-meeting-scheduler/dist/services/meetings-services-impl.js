"use strict";
exports.__esModule = true;
exports.MeetingsServiceImpl = void 0;
var meetings_dao_postgres_1 = require("../DAOs/meetings-dao-postgres");
var MeetingsServiceImpl = /** @class */ (function () {
    function MeetingsServiceImpl() {
        this.meetingsDAO = new meetings_dao_postgres_1.MeetingsDaoPostgress();
    }
    MeetingsServiceImpl.prototype.createMeeting = function (meeting) {
        return this.meetingsDAO.createMeeting(meeting);
    };
    MeetingsServiceImpl.prototype.retrieveAllMeetings = function () {
        return this.meetingsDAO.getAllMeetings();
    };
    MeetingsServiceImpl.prototype.retieveMeetingById = function (meeting_id) {
        return this.meetingsDAO.getMeetingById(meeting_id);
    };
    MeetingsServiceImpl.prototype.updateMeeting = function (meeting) {
        return this.meetingsDAO.updateMeeting(meeting);
    };
    MeetingsServiceImpl.prototype.deleteMeeting = function (meeting_id) {
        return this.meetingsDAO.deleteMeetingById(meeting_id);
    };
    return MeetingsServiceImpl;
}());
exports.MeetingsServiceImpl = MeetingsServiceImpl;
