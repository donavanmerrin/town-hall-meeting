import express from 'express'
import { Meetings } from './entities'
import { MissingMeetingError } from './errors';

import MeetingsService from './services/meetings-services';
import { MeetingsServiceImpl } from './services/meetings-services-impl';

const cors = require('cors');
const app = express();
app.use(express.json());
app.use(cors());


const meetingsService:MeetingsService = new MeetingsServiceImpl();

app.get('/meetings', async (req, res)=>{
    const meetings:Meetings[] = await meetingsService.retrieveAllMeetings();
    res.send(meetings);
    res.status(200);
});


app.get('/meetings/:id', async (req, res)=>{
    try{
        const meetingId = Number(req.params.id);
        const meeting:Meetings = await meetingsService.retieveMeetingById(meetingId);
        res.send(meeting);
    } catch (error) {
        if (error instanceof MissingMeetingError){
            res.status(404);
            res.send(MissingMeetingError);
        }
    }
});


app.put('/meetings/:id', async (req, res)=>{
    try{
        const auth = req.headers.authorization;
        if (auth !== "funky_bandit"){
            res.status(405);
            res.send("Not authorized to perform this action");
        }
        const meetingId = Number(req.params.id);
        const meet:Meetings = req.body;
        meet.meeting_id = meetingId;
        const updatedMeeting = await meetingsService.updateMeeting(meet);
        await meetingsService.updateMeeting(updatedMeeting);
        res.send(updatedMeeting);
        res.status(200);
    } catch (error) {
        if (error instanceof MissingMeetingError){
            res.status(404);
            res.send(MissingMeetingError);
        }
    }
});


app.post('/meetings', async(req, res)=>{
    try {
        const auth = req.headers.authorization;
        if (auth !== "funky_bandit"){
            res.status(405);
            res.send("Not authorized to perform this action");
        }
    let meeting:Meetings = req.body;
    meeting = await meetingsService.createMeeting(meeting);
    res.send(meeting);
    } catch (error) {
        if (error instanceof MissingMeetingError){
            res.status(404);
            res.send(MissingMeetingError);
        }
    }
});



app.delete('/meetings/:id', async(req, res)=>{
    try{
        const auth = req.headers.authorization;
        if (auth !== "funky_bandit"){
            res.status(405);
            res.send("Not authorized to perform this action");
        }
        const meetingId = Number(req.params.id);
        await meetingsService.deleteMeeting(meetingId);
        res.send(`${meetingId} has been deleted.`);
    } catch (error) {
        if (error instanceof MissingMeetingError){
            res.status(404);
            res.send(MissingMeetingError);
        }
    }

});

app.listen(process.env.PORT || 3001, ()=>{
    console.log("Application Started!")
});