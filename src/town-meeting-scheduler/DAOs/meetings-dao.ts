import { Meetings } from "../entities";

export interface meetingsDAO{

    createMeeting(meeting:Meetings):Promise<Meetings>;

    getMeetingById(meeting_id:number):Promise<Meetings>;
    getAllMeetings():Promise<Meetings[]>;

    // PUT
    updateMeeting(meeting:Meetings):Promise<Meetings>;

    // DELETE
    deleteMeetingById(meeting_id:number): Promise<boolean>;
    
}