
export class Meetings{
    constructor( 
        public meeting_id: number,
        public location: string,
        public time: string,
        public topic: string
    ){}
}