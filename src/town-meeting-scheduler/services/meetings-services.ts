import { Meetings } from "../entities"

export default interface MeetingsService{
 
    createMeeting(meeting:Meetings):Promise<Meetings>; 

    retrieveAllMeetings():Promise<Meetings[]>;

    retieveMeetingById(meeting_id:number):Promise<Meetings>;

    updateMeeting(meeting:Meetings):Promise<Meetings>;

    deleteMeeting(meeting_id:number):Promise<boolean>;
}