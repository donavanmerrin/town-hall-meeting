// import MeetingsTable2 from "./MeetingsTable2";
import axios from "axios";
import { useState, useRef } from "react";
import MeetingsTable2 from "./MeetingsTable2";

export default function CreateMeeting(){
    
    const locationInput = useRef(null);
    const timeInput = useRef(null);
    const topicInput = useRef(null);
    
    const [meetings, setMeetings] = useState([]);


    async function getMeetings(){
        const response = await axios.get('http://localhost:3002/meetings');
            
        setMeetings(response.data);
        console.log(meetings);
    }

    getMeetings();

    async function createMeeting(){
        
        const meeting = {
            location: locationInput.current.value,
            time: timeInput.current.value,
            topic: topicInput.current.value
        }
        
        localStorage.setItem("token", "funky_bandit");

        const response = await axios.post('http://localhost:3002/meetings', meeting, 
        {
            headers: {
                authorization: localStorage.getItem("token")
            }
        }
    );
}

    return(<div>
        
        <MeetingsTable2 meetings={meetings}></MeetingsTable2>

        <h1>Create Meeting</h1>
        <form>
            <input ref={locationInput}></input>
            <select ref={timeInput}>
                    <option value="12:00pm">12:00pm</option>
                    <option value="12:30pm">12:30pm</option>
                    <option value="1:00pm">1:00pm</option>
                    <option value="1:00pm">1:30pm</option>
                    <option value="2:00pm">2:00pm</option>
                    <option value="2:00pm">2:30pm</option>
                    <option value="3:00pm">3:00pm</option>
                    <option value="3:00pm">3:30pm</option>
                    <option value="4:00pm">4:00pm</option>
                    <option value="4:00pm">4:30pm</option>
                    <option value="5:00pm">5:00pm</option>
                    <option value="5:00pm">5:30pm</option>
                    <option value="6:00pm">6:00pm</option>
                    <option value="6:00pm">6:30pm</option>
                    <option value="7:00pm">7:00pm</option>
                    <option value="7:00pm">7:30pm</option>
            </select>
            <select ref={topicInput}>
                    <option value="Pollution">Pollution</option>
                    <option value="Noise">Noise</option>
                    <option value="Infrastructure">Infrastructure</option>
                    <option value="Other">Other</option>
            </select>
            <button onClick={createMeeting}>Submit Meeting</button>
        </form>
        </div>)
}