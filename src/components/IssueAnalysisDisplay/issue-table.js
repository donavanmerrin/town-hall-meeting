import React, {useState} from "react";
export default function IssuesTable(props){

    const issues = props.issues
    console.log(props.issues)
    return(<table>
        <thead><tr>
        <th>Type of Issue</th>
        <th>Date Posted</th>
        <th>Date of Issue</th>
        <th>Reviewed</th>
        <th>Highlighted</th>
             </tr>
        </thead>
        <tbody>
            {issues.map(i=><tr>
                <td>{i.type}</td>
                <td>{i.date_of_posting}</td>
                <td>{i.date_of_issue}</td>
                <td>{i.reviewed === true ? "true" : "false" }</td>
                <td>{i.highlighted === true ? "true" : "false" }</td>
            </tr>)}
        </tbody>
    </table>)


}