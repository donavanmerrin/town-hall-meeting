import axios from "axios";
import { useEffect, useState } from "react";
import MeetingsTable from "./MeetingsTable";

export default function MeetingsPage(props){

    const [meetings, setMeetings] = useState([]);


    async function getMeetings(){
        const response = await axios.get('http://localhost:3002/meetings');
            
        setMeetings(response.data);
    }
        useEffect(() => getMeetings());

    async function goToCreate(){
        
    }
    
    return(<div>
        <h1>Funkytown Meetings</h1>
        <MeetingsTable meetings={meetings}></MeetingsTable>

        <a href="/createMeeting"><button onClick={goToCreate}>Create/Edit Meetings</button></a>
    </div>)
    }