import React, {useState} from "react";
import axios from "axios";
import "./NewIssue.css";
import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from 'moment-timezone';

export default function NewIssue() {
    
    const [highlighted, setHighlighted] = useState('');
    const [description, setDescription] = useState('');
    const [typeOfIssue, setTypeOfIssue] = useState('Infrastructure');
    const [location, setLocation] = useState('');
    const [dateOfIssue, setDateOfIssue] = useState('');
    const [dateOfIssuePosted, setDateOfIssuePosted] = useState('');
    moment.tz.setDefault("America/Los_Angeles");

    //use priority value radio button to set highlight value
    function onUrgentClicked(e) {
      setHighlighted(e.target.value);
    }

    function onIssueTypeChange(e){
      setTypeOfIssue(e.target.value);
    }

    function onDescriptionChange(e){
      setDescription(e.target.value);
    }

    function onLocationChanges(e){
      setLocation(e.target.value);
    }

    async function handleSubmitIssue(e){
      const setPriority = highlighted === 'on' ? true: false;
      console.log(setPriority);
      //check the date OfIssue
      // check the date ofIssuePost - if in ISO format or not
      const issue = {
        "issue_description": description,
        "type" : typeOfIssue,
        "highlighted" : setPriority,
        "date_of_issue" : dateOfIssue,
        "date_of_posting" : dateOfIssuePosted,
        "reviewed" : false,
        "location" : location
      }
      
      const response = await axios.post('https://backend-v9-7npnpkiefa-uc.a.run.app/submitissue', issue);

      alert('Message sent');
      //axios.post


    }
    
  return (
    <div class ="container">
      <form >
        <div className="form-group">
          <label>
            Description
            <input class="form-control" onChange={onDescriptionChange} type="text" placeholder="Description of Issue..." />
          </label>
        </div>
	      <div className="form-group">
          <label>Location of issue
	        <input class="form-control" onChange={onLocationChanges} type="text" placeholder="StreetName or Landmark"/>
	        </label>
	      </div>

        <div >
          <label>Date of issue</label>
	        <DatePicker  format="YYYY-MM-D HH:m:s" selected={dateOfIssue} onChange={date => setDateOfIssue(date)} />
	      </div>

        <div>
          <label>Date of Posting Issue</label>
	        <DatePicker format="YYYY-MM-D HH:m:s" selected={dateOfIssuePosted} onChange={date => setDateOfIssuePosted(date)} />
	        
	      </div>

        <div className="form-group">
          <label>
            Issue Type
          <select onSelect={onIssueTypeChange} onChange={onIssueTypeChange} class="form-control" name="issueType" id="issueType">
            <option value="Infrastructure">Infrastructure</option>
            <option value="Safety">Safety</option>
            <option value="PublicHealth"> Public Health</option>
            <option value="Pollution"> Pollution</option>
            <option value="Noise"> Noise </option>
            <option value="Other"> Other </option>
          </select>
          </label>
        </div>
        <div className="form-group">
        <label>Priority / Highlighted </label>
          <input  type="radio" name="address" 
              onChange={onUrgentClicked} />
        <div></div>
        <button type="button" class="btn btn-primary" onClick={handleSubmitIssue}>Submit Issue</button>
        
        </div>
      </form>
    </div>
  );
}

