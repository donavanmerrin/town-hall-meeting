import axios from "axios";
import { useState, useRef } from "react";
import { useParams } from "react-router";

export default function EditMeeting(props){

    const [meeting, setMeeting] = useState([]);

    const locationInput = useRef(null);
    const timeInput = useRef(null);
    const topicInput = useRef(null);

    const { id } = useParams();

    async function getMeeting(id){
        const response = await axios.get(`http://localhost:3002/meetings/${id}`);
        setMeeting(response.data);
    }

    getMeeting(id);

    async function editMeeting(){
        
        const newMeeting = {
            location: locationInput.current.value,
            time: timeInput.current.value,
            topic: topicInput.current.value
        }

        const response = await axios.put(`http://localhost:3002/meetings/${id}`, newMeeting, 
        {
            headers: {
                authorization: localStorage.setItem("token", "funky_bandit")
            }
        }
    );
    setMeeting(response.data);
}

    return(<div>
        <h1>Meeting {meeting.meeting_id}</h1>


            <form>
                <input placeholder={meeting.location} ref={locationInput}></input>
                <select placeholder={meeting.time} ref={timeInput}>
                    <option value="12:00pm">12:00pm</option>
                    <option value="12:30pm">12:30pm</option>
                    <option value="1:00pm">1:00pm</option>
                    <option value="1:00pm">1:30pm</option>
                    <option value="2:00pm">2:00pm</option>
                    <option value="2:00pm">2:30pm</option>
                    <option value="3:00pm">3:00pm</option>
                    <option value="3:00pm">3:30pm</option>
                    <option value="4:00pm">4:00pm</option>
                    <option value="4:00pm">4:30pm</option>
                    <option value="5:00pm">5:00pm</option>
                    <option value="5:00pm">5:30pm</option>
                    <option value="6:00pm">6:00pm</option>
                    <option value="6:00pm">6:30pm</option>
                    <option value="7:00pm">7:00pm</option>
                    <option value="7:00pm">7:30pm</option>
                </select>
                <select placeholder={meeting.topic} ref={topicInput}>
                    <option value="Pollution">Pollution</option>
                    <option value="Noise">Noise</option>
                    <option value="Infrastructure">Infrastructure</option>
                    <option value="Other">Other</option>
                </select>
                <button onClick={editMeeting}>Submit Meeting</button>
            </form>

            <h3>Current Time: {meeting.time}</h3>
            <h3>Current Topic: {meeting.topic}</h3>
        </div>)

}