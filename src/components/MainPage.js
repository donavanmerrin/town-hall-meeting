import React, {useState, useEffect} from "react";
import NewIssue from "./NewIssue/NewIssue";
import CurrentIssue from "./CurrentIssue/CurrentIssue";
import background from "../images/FunkyTown-Background.jpeg"
import {BrowserRouter as Router,  Link} from "react-router-dom";
import axios from "axios";

export default function MainPage(){
    const[newIssue, setNewIssue]  = useState(true);

    function showNewIssue() {
        setNewIssue(true);
    }
    
    function showCurrentIssue() {
        setNewIssue(false);
    }

    useEffect( () => {
        axios.get('https://pub-sub-subscriber-7npnpkiefa-uc.a.run.app/issues');
    }, [])

    return (
        <div className="MainPage" style={{backgroundImage: `url(${background})`, backgroundRepeat: "no-repeat", backgroundPosition: "center", backgroundSize: "cover" }}>
            <h1> FunkyTown Issue Tracker</h1>
            <div className = "grey-card-container">
                {newIssue ? <NewIssue/> : <CurrentIssue/>}
            </div>
            <form>
                <div>
                <Link class="nav-link" to="/currentIssues" >Current Issues</Link>  
                
                <Link class="nav-link" to="/analysis"> Analysis Page Search </Link>  
                <button class="btn btn-success" onClick = {() => showNewIssue()}> 
                    Add Issue
                </button>
                <Link to="/meetings">
                    <button class="btn btn-info"> View Meetings</button>
                </Link>
                </div>
            </form>
        </div>
    )
}