import { Issues } from "../entities";

export interface issuesDAO{

    createIssue(issue:Issues):Promise<Issues>;
    
}