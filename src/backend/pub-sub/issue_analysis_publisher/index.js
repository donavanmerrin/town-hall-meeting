const bunyan = require('bunyan');

const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'funky-town-326721'})
// Imports the Google Cloud client library for Bunyan.
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');
// Creates a Bunyan Cloud Logging client
const loggingBunyan = new LoggingBunyan();

// Import express module and create an http server.
const express = require('express');

const logger = bunyan.createLogger({
    // The JSON payload of the log as it appears in Cloud Logging
    // will contain "name": "my-service"
    name: 'issue-ingestion-service',
    streams: [
      // Log to the console at 'info' and above
      {stream: process.stdout, level: 'info'},
      // And log to Cloud Logging, logging at 'info' and above
      loggingBunyan.stream('info'),
    ],
  });

const app = express();

// Install the logging middleware. This ensures that a Bunyan-style `log`
// function is available on the `request` object. This should be the very
// first middleware you attach to your app.


const cors = require("cors"); 

app.use(express.json());
app.use(cors());

app.post('/submitissue', async(req, res) => {
    console.log("/submitissue - posting an issue");
    
    logger.info('/submitissue: '+req.body)
    const obj = req.body;
    pubsub.topic('issues').publishJSON(obj); 
    res.status(201)
    res.send("Issue sent");
})


const PORT = process.env.PORT || 3000
app.listen(PORT, ()=>console.log('Application Started'))

