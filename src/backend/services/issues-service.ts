import { Issues } from "../entities";

export default interface IssuesService{

    createIssue(issue:Issues):Promise<Issues>;

    retrieveAllIssues():Promise<Issues[]>;
    retrieveIssuesByType(typeOfIssue:string):Promise<Issues>;
    retrieveIssuesByDatePosted(dateOfPosting:string):Promise<Issues>;
    retrieveIssuesByDateOfIssue(dateOfIssue:string):Promise<Issues>;
    retrieveIssuesByReviewed(issueReviewed:boolean):Promise<boolean>;
    retrieveIssuesByHighlighted(issueHighlighted:boolean):Promise<boolean>;
    
}