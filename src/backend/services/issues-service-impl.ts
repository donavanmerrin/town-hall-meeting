import { issuesDAO } from "../DAOs/issues-dao";
import { Issues } from "../entities";
import IssuesService from "./issues-service";


export class IssuesServiceImpl implements IssuesService{


    createIssue(issue: Issues): Promise<Issues> {
        throw new Error("Method not implemented.");
    }

    retrieveAllIssues(): Promise<Issues[]> {
        // return this.issuesDao.retrieveAllIssues();
        throw new Error("Method not implemented.");
    }
    retrieveIssuesByType(typeOfIssue: string): Promise<Issues> {
        throw new Error("Method not implemented.");
    }
    retrieveIssuesByDatePosted(dateOfPosting: string): Promise<Issues> {
        throw new Error("Method not implemented.");
    }
    retrieveIssuesByDateOfIssue(dateOfIssue: string): Promise<Issues> {
        throw new Error("Method not implemented.");
    }
    retrieveIssuesByReviewed(issueReviewed: boolean): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    retrieveIssuesByHighlighted(issueHighlighted: boolean): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    retrieveIssuesReport(issueReport: any) {
        throw new Error("Method not implemented.");
        //
    }
    
}
